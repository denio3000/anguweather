import {Component} from '@angular/core';
import {NgForm} from '@angular/forms';
import { HttpClientService } from './httpclient.service';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  appTitle = 'anguWeather';
  keyword: string;
  data: any;

  constructor(private router: Router, private http: HttpClientService) {
  }

  onSubmit(f: NgForm) {
    if (f.valid && f.value.keyword.length > 2) {
      this.data = {command: 'search', var: 'keyword', val: f.value.keyword};

      this.http.post(this.data).subscribe((result: any) => {
        if (result.length > 0) {
          this.router.navigate(['./weather/', result[0].woeid]);
        } else {
          alert('Location not Found!');
        }
      });
    }
  }
}
