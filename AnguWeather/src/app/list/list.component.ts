import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClientService} from '../httpclient.service';
import {WEATHERS} from '../home-weathers';
import {Injectable} from '@angular/core';

@Injectable()

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  weathers = WEATHERS;
  data: {};

  constructor(private router: Router, private http: HttpClientService) {

    this.weathers.forEach(childObj => {
      this.data = {command: 'location', var: 'woeid', val: childObj.woeid};

      this.http.post(this.data).subscribe((result: any) => {
        if (result.consolidated_weather) {
          childObj.today.temperature = Math.round(result.consolidated_weather[0].the_temp);
          childObj.today.max = Math.round(result.consolidated_weather[0].max_temp);
          childObj.today.min = Math.round(result.consolidated_weather[0].min_temp);
          childObj.today.ico = 'https://www.metaweather.com/static/img/weather/' + result.consolidated_weather[0].weather_state_abbr + '.svg';
        }
      });
    });
  }

  ngOnInit() {
  }

  navigateToItem(woeid) {
    this.router.navigate(['./weather/', woeid]);
  }
}
