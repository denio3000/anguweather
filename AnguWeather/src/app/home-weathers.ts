import { Weather } from './weather';

export const WEATHERS: Weather[] = [
  {
    woeid: '2344116',
    city: 'Istanbul',
    today: { temperature: null, max: null, min: null, ico: null },
    secondday: { temperature: null, max: null, min: null, ico: null }
  },
  {
    woeid: '638242',
    city: 'Berlin',
    today: { temperature: null, max: null, min: null, ico: null },
    secondday: { temperature: null, max: null, min: null, ico: null }
  },
  {
    woeid: '44418',
    city: 'London',
    today: { temperature: null, max: null, min: null, ico: null },
    secondday: { temperature: null, max: null, min: null, ico: null }
  },
  {
    woeid: '565346',
    city: 'Helsinki',
    today: { temperature: null, max: null, min: null, ico: null },
    secondday: { temperature: null, max: null, min: null, ico: null }
  },
  {
    woeid: '560743',
    city: 'Dublin',
    today: { temperature: null, max: null, min: null, ico: null },
    secondday: { temperature: null, max: null, min: null, ico: null }
  },
  {
    woeid: '9807',
    city: 'Vancouver',
    today: { temperature: null, max: null, min: null, ico: null },
    secondday: { temperature: null, max: null, min: null, ico: null }
  }
];
