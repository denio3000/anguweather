import {Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Http, Headers} from '@angular/http';

@Injectable()
export class HttpClientService {
  serverUrl = 'http://localhost/anguweather/weather.php';

  headers = new HttpHeaders({
      'Access-Control-Allow-Headers': 'Content-Type',
      'Access-Control-Allow-Methods': 'GET,POST',
      'Access-Control-Allow-Origin': '*'
  });

  constructor(private http: HttpClient) {}

  get(data) {
    console.log(this.headers);
    return this.http.get(this.serverUrl + '?command=' + data.command + '&' + data.var + '=' + data.val, {
      headers: this.headers
    });
  }

  post(data) {
    const headers = new HttpHeaders();
    return this.http.post(this.serverUrl + '?command=' + data.command + '&' + data.var + '=' + data.val, {
      headers: headers
    });
  }
}
