export class Weather {
  woeid: string;
  city: string;
  today: {
    temperature: number;
    max: number;
    min: number;
    ico: string;
  };
  secondday: {
    temperature: number;
    max: number;
    min: number;
    ico: string;
  };
}

