import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Weather} from '../weather';
import {HttpClientService} from '../httpclient.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  data: any;
  woeid: string;
  img_url = 'https://www.metaweather.com/static/img/weather/';
  weather: Weather;

  constructor(private route: ActivatedRoute, private http: HttpClientService) {

    route.params.subscribe((params: any) => {
      this.woeid = params.woeid;
      this.data = {command: 'location', var: 'woeid', val: this.woeid};

      this.weather = {
        woeid: this.woeid.toString(),
        city: null,
        today: {temperature: null, max: null, min: null, ico: null},
        secondday: {temperature: null, max: null, min: null, ico: null}
      };

      this.http.post(this.data).subscribe((result: any) => {
        this.weather.city = result.title;
        if (result.consolidated_weather) {
          this.weather.today.temperature = Math.round(result.consolidated_weather[0].the_temp);
          this.weather.today.max = Math.round(result.consolidated_weather[0].max_temp);
          this.weather.today.min = Math.round(result.consolidated_weather[0].min_temp);
          this.weather.today.ico = this.img_url + result.consolidated_weather[0].weather_state_abbr + '.svg';

          this.weather.secondday.temperature = Math.round(result.consolidated_weather[1].the_temp);
          this.weather.secondday.max = Math.round(result.consolidated_weather[1].max_temp);
          this.weather.secondday.min = Math.round(result.consolidated_weather[1].min_temp);
          this.weather.secondday.ico = this.img_url + result.consolidated_weather[1].weather_state_abbr + '.svg';
        }
      });
    });
  }

  ngOnInit() {
  }
}
