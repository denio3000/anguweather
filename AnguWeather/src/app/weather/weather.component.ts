import {Component, Input, OnInit} from '@angular/core';
import { Weather } from '../weather';

@Component({
  selector: 'weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss']
})
export class WeatherComponent implements OnInit {
  @Input() weather: Weather;

  constructor() {
  }

  ngOnInit() {
  }
}
