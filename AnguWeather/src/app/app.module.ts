import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WeatherComponent } from './weather/weather.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './item/item.component';
import { RouterModule, Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HttpClientService } from './httpclient.service';

/** Define Routes **/
const appRoutes: Routes = [
  { path: 'weather/:woeid', component: ItemComponent },
  { path: 'search/:keyword', component: SearchComponent },
  { path: '', component: ListComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    WeatherComponent,
    ListComponent,
    ItemComponent,
    SearchComponent,
    PageNotFoundComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes
      /*,{ enableTracing: true } */
    ),
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [
    HttpClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
