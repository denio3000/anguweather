import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClientService} from '../httpclient.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  keyword: string;
  data: any;

  constructor(private router: Router, private route: ActivatedRoute, private http: HttpClientService) {
    route.params.subscribe((params: any) => {
      this.keyword = params.keyword;

      if (this.keyword.length > 2) {
        this.data = {command: 'search', var: 'keyword', val: this.keyword};

        this.http.post(this.data).subscribe((result: any) => {
          if (result.length > 0) {
            /* Redirect to item page if found */
            this.router.navigate(['./weather/', result[0].woeid]);
          } else {
            alert('Location not Found!');
          }
        });
      }
    });
  }

  ngOnInit() {
  }

}
